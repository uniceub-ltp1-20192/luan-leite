from Entidades.imovel import Imovel


class Casa(Imovel):

    def __str__(self):
        return """ Dados do Imóvel:
        Identificador: {}
        Imobiliaria = {}
        Endereço = {}
        Ano = {}
        Tamanho = {}

        """.format(self.identificador,

                   self.imobiliaria,
                   self.endereco,
                   self.ano,
                   self.tamanho,
                   )
