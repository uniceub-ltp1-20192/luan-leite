class Imovel:

    def __init__(self, identificador=0, imobiliaria="", endereco="", ano=0, tamanho=""):
        self._identificador = identificador
        self._imobiliaria = imobiliaria
        self._endereco = endereco
        self._ano = ano
        self._tamanho = tamanho

    @property
    def identificador(self):
        return self._identificador

    @identificador.setter
    def identificador(self, identificador):
        self._identificador = identificador

    @property
    def imobiliaria(self):
        return self._imobiliaria

    @imobiliaria.setter
    def imobiliaria(self, imobiliaria):
        self._imobiliaria = imobiliaria

    @property
    def endereco(self):
        return self._endereco

    @endereco.setter
    def endereco(self, endereco):
        self._endereco = endereco

    @property
    def ano(self):
        return self._ano

    @ano.setter
    def ano(self, ano):
        self._ano = ano

    @property
    def tamanho(self):
        return self._tamanho

    @tamanho.setter
    def tamanho(self, tamanho):
        self._tamanho = tamanho
