import sys

# Apresenta linhas
def linhas():
    print('-' * 50)


#Perguntar e Validar
def askNota():
 try:
    num_grades = int(input("Quantidade de Notas: "))
 except(ValueError):
    print("Não são números válidos")
 if num_grades > 10 or num_grades < 0:
     print("Qntd de notas Inválida")
     sys.exit(1)
 return(num_grades)


#Tirar num_grades do def
num_grades = askNota()

# para a quantidade de notas solicita a nota e peso
sum_grades = 0
sum_weighted_grades = 0
sum_weighted = 0

for i in range(num_grades):
   try:
    grade = float(input("Nota: "))
    weight = int(input("Peso (1-5): "))
   except(ValueError):
       print("Digite um número, por favor.")
       sys.exit(1)
   if grade > 10 or grade < 0 and weight > 6 or weight < 0:
      print("Digite um número válido")
      sys.exit(1)
   #adiciona a soma das notas
   sum_grades = sum_grades + grade

   # adiciona a soma das notas com peso
   sum_weighted_grades = sum_weighted_grades + (grade * weight)

   # adiciona a soma de pesos
   sum_weighted = sum_weighted + weight


# Apresenta o resultado final
def resultado():
    print("Média: ", sum_grades / num_grades)
    print("Média ponderada: ", sum_weighted_grades / sum_weighted)


linhas()
resultado()